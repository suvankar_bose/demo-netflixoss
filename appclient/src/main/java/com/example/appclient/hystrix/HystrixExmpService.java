package com.example.appclient.hystrix;


import com.netflix.hystrix.contrib.javanica.annotation.HystrixCommand;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

@Service
public class HystrixExmpService {

	@Autowired
	private RestTemplate restTemplate;



	@HystrixCommand(fallbackMethod = "getDefaultVal")
	public String getResultFromService(){
		return restTemplate.getForEntity("http://appservice/",String.class).getBody();
	}

	private String getDefaultVal(){
		return " unknown server";
	}
}
