package com.example.appclient.hystrix;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cloud.client.circuitbreaker.EnableCircuitBreaker;
import org.springframework.cloud.client.loadbalancer.LoadBalanced;
import org.springframework.context.annotation.Bean;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

@RestController
@EnableCircuitBreaker
public class HystrixExmpController {

	@Autowired
	private HystrixExmpService hystrixExmpService;

	@RequestMapping("/getHiFromHystrix")
	public String sayHiFromService(){
		return hystrixExmpService.getResultFromService();
	}


}
