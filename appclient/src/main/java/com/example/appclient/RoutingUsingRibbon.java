package com.example.appclient;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cloud.client.loadbalancer.LoadBalanced;
import org.springframework.context.annotation.Bean;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

@RestController
public class RoutingUsingRibbon {

	@Autowired
	RestTemplate restTemplate;


	@RequestMapping("/helloUsingRibbon")
	public String callServiceUsingRibbon(){

		return restTemplate.getForEntity("http://appservice/",String.class).getBody();
	}

	@Bean
	@LoadBalanced
	public RestTemplate initalizeRestTemplate(){
		return new RestTemplate();

	}
}
