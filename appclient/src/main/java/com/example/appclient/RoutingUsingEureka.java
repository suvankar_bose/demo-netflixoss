package com.example.appclient;

import com.netflix.appinfo.InstanceInfo;
import com.netflix.discovery.EurekaClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;
@RestController
public class RoutingUsingEureka {
	@Autowired
	private EurekaClient eurekaClient;

	@Autowired
	private RestTemplateBuilder restTemplateBuilder;

	@RequestMapping("/hello")
	public String callService(){
		RestTemplate restTemplate = restTemplateBuilder.build();
		InstanceInfo instanceInfo = eurekaClient.getNextServerFromEureka("appservice" , false);
		String baseUrl = instanceInfo.getHomePageUrl();

		ResponseEntity<String> responseEntity = restTemplate.exchange(baseUrl,
				HttpMethod.GET,null,String.class);

		return responseEntity.getBody();



	}
}
